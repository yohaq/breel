package jsf;

import jpa.entities.Aliases;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.AliasesFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("aliasesController")
@SessionScoped
public class AliasesController implements Serializable {

    private Aliases current;
    private DataModel items = null;
    @EJB
    private jpa.session.AliasesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public AliasesController() {
    }

    public Aliases getSelected() {
        if (current == null) {
            current = new Aliases();
            current.setAliasesPK(new jpa.entities.AliasesPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private AliasesFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Aliases) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Aliases();
        current.setAliasesPK(new jpa.entities.AliasesPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getAliasesPK().setUserId(current.getUsers().getUserId());
            current.getAliasesPK().setPostId(current.getPosts().getPostId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AliasesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Aliases) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getAliasesPK().setUserId(current.getUsers().getUserId());
            current.getAliasesPK().setPostId(current.getPosts().getPostId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AliasesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Aliases) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AliasesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Aliases getAliases(jpa.entities.AliasesPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Aliases.class)
    public static class AliasesControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AliasesController controller = (AliasesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "aliasesController");
            return controller.getAliases(getKey(value));
        }

        jpa.entities.AliasesPK getKey(String value) {
            jpa.entities.AliasesPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.AliasesPK();
            key.setAlias(values[0]);
            key.setUserId(Integer.parseInt(values[1]));
            key.setPostId(Integer.parseInt(values[2]));
            return key;
        }

        String getStringKey(jpa.entities.AliasesPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getAlias());
            sb.append(SEPARATOR);
            sb.append(value.getUserId());
            sb.append(SEPARATOR);
            sb.append(value.getPostId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Aliases) {
                Aliases o = (Aliases) object;
                return getStringKey(o.getAliasesPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Aliases.class.getName());
            }
        }

    }

}
