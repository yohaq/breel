package jsf;

import jpa.entities.Votes;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.VotesFacade;

import java.io.Serializable;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("votesController")
@SessionScoped
public class VotesController implements Serializable {

    private Votes current;
    private DataModel items = null;
    @EJB
    private jpa.session.VotesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public VotesController() {
    }

    public Votes getSelected() {
        if (current == null) {
            current = new Votes();
            current.setVotesPK(new jpa.entities.VotesPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private VotesFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Votes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Votes();
        current.setVotesPK(new jpa.entities.VotesPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getVotesPK().setCommentId(current.getComments().getCommentId());
            current.getVotesPK().setUserId(current.getUsers().getUserId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VotesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Votes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getVotesPK().setCommentId(current.getComments().getCommentId());
            current.getVotesPK().setUserId(current.getUsers().getUserId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VotesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Votes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VotesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Votes getVotes(jpa.entities.VotesPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Votes.class)
    public static class VotesControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            VotesController controller = (VotesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "votesController");
            return controller.getVotes(getKey(value));
        }

        jpa.entities.VotesPK getKey(String value) {
            jpa.entities.VotesPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.VotesPK();
            key.setCommentId(Integer.parseInt(values[0]));
            key.setUserId(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(jpa.entities.VotesPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getCommentId());
            sb.append(SEPARATOR);
            sb.append(value.getUserId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Votes) {
                Votes o = (Votes) object;
                return getStringKey(o.getVotesPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Votes.class.getName());
            }
        }

    }
    
    public void castCommentVote(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map map = context.getExternalContext().getRequestParameterMap();
        String userId = (String) map.get("userId");
        String commentId = (String) map.get("commentId");
        String voteValue = (String) map.get("voteValue");
        
    
        ejbFacade.castVote(userId, commentId, voteValue);

    
    }
    
    public void deleteVote(){
        FacesContext context = FacesContext.getCurrentInstance();
        Map map = context.getExternalContext().getRequestParameterMap();
        String userId = (String) map.get("userId");
        String commentId = (String) map.get("commentId");
        
        
    
        ejbFacade.deleteVote(userId, commentId);

    
    }

}
