package jsf;

import jpa.entities.PollVotes;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import jpa.session.PollVotesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;


@Named("pollVotesController")
@SessionScoped
public class PollVotesController implements Serializable {


    private PollVotes current;
    private DataModel items = null;
    @EJB private jpa.session.PollVotesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private int currentVote = -1;
    

    public PollVotesController() {
    }

    public PollVotes getSelected() {
        if (current == null) {
            current = new PollVotes();
            current.setPollVotesPK(new jpa.entities.PollVotesPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private PollVotesFacade getFacade() {
        return ejbFacade;
    }
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem()+getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (PollVotes)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new PollVotes();
        current.setPollVotesPK(new jpa.entities.PollVotesPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getPollVotesPK().setPostId(current.getPosts().getPostId());
            current.getPollVotesPK().setUserId(current.getUsers().getUserId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PollVotesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (PollVotes)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getPollVotesPK().setPostId(current.getPosts().getPostId());
            current.getPollVotesPK().setUserId(current.getUsers().getUserId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PollVotesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (PollVotes)getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PollVotesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count-1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex+1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public PollVotes getPollVotes(jpa.entities.PollVotesPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass=PollVotes.class)
    public static class PollVotesControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PollVotesController controller = (PollVotesController)facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "pollVotesController");
            return controller.getPollVotes(getKey(value));
        }

        jpa.entities.PollVotesPK getKey(String value) {
            jpa.entities.PollVotesPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.PollVotesPK();
            key.setUserId(Integer.parseInt(values[0]));
            key.setPostId(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(jpa.entities.PollVotesPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getUserId());
            sb.append(SEPARATOR);
            sb.append(value.getPostId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof PollVotes) {
                PollVotes o = (PollVotes) object;
                return getStringKey(o.getPollVotesPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: "+PollVotes.class.getName());
            }
        }

    }

    public int getCurrentVote() {
        return currentVote;
    }

    public void setCurrentVote(int currentVote) {
        this.currentVote = currentVote;
    }
    
    public void castVote(int userid, int postid){
        
        
        ejbFacade.castVote(userid, postid, currentVote);
        currentVote = -1;
    }
    
   public void getVoteForPostByUser(int userid, int postid){
       
        
        List<PollVotes> result =  ejbFacade.getVoteForPostByUser(userid, postid);
        if(result.size()>0){
            currentVote = result.get(0).getSeqno();
        }
        else currentVote = -1;
        
         
        
    }
}
