/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Posts;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class PostsFacade extends AbstractFacade<Posts> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostsFacade() {
        super(Posts.class);
    }
    
    public DataModel getPostsOrderedByDate(){
        return new ListDataModel(em.createNamedQuery("Posts.findAllOrderedByDate").getResultList());
    }
}
