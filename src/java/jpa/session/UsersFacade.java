/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import Models.CommentsTreeNodeInfo;
import Models.LeaderboardStats;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Users;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }
    
    public List<LeaderboardStats> getLeaderboardStats() {

        List<Object[]> leaderBoardStats = em.createNativeQuery("select total.user_id, email, count(comment_id) as numcomment, sum(totalvotes) as totalvotes from (select *, netvotes*has_link as totalvotes  from (select *, upvotes-downvotes as netvotes from (select comments.comment_id, user_id, has_link, coalesce(upvotes, 0) as upvotes, coalesce(downvotes, 0) as downvotes from newsgames.comments \n" +
"left join \n" +
"(select  comment_id, count(value) as upvotes from newsgames.votes where value='up' group by comment_id)	as upvotestats\n" +
"on upvotestats.comment_id = comments.comment_id\n" +
"left join \n" +
"(select comment_id, count(value) as downvotes from newsgames.votes where value='down' group by comment_id)	as downvotestats\n" +
"on downvotestats.comment_id = comments.comment_id) as stats) as calculatedstats)as total \n" +
"left join newsgames.users on total.user_id = users.user_id\n" +
"group by user_id").getResultList();
        List<LeaderboardStats> statList = new ArrayList<LeaderboardStats>();

        Object[] singleTopLevelComment;
        for (int i = 0; i < leaderBoardStats.size(); i++) {
            singleTopLevelComment = leaderBoardStats.get(i);
            
            Integer user_id = (Integer) singleTopLevelComment[0];
            String email = (String) singleTopLevelComment[1];
            Long numComments =(Long) singleTopLevelComment[2];
             Double totalUpvotes =(Double) singleTopLevelComment[3];
            
            LeaderboardStats info = new LeaderboardStats(
                    user_id,
                    email,
                    numComments,
                    totalUpvotes);
            statList.add(info);
          
        }
        return statList;

    }
    
    public int getIDbyEmail(String email){
        List<Object[]> users = em.createNativeQuery("SELECT user_id, email, name FROM newsgames.users where lower(email)=lower(?)").setParameter(1, email).getResultList();
        int userid = 0;
        String name;
        if(users.size()>0){
            userid = (Integer) users.get(0)[0];
            name = (String) users.get(0)[2];
                    
        }
        return userid;
    }
}
