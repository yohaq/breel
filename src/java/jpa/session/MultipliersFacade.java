/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Multipliers;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class MultipliersFacade extends AbstractFacade<Multipliers> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MultipliersFacade() {
        super(Multipliers.class);
    }
    
}
