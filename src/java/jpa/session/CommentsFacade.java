/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import Models.CommentsTreeNodeInfo;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Comments;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class CommentsFacade extends AbstractFacade<Comments> {

    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommentsFacade() {
        super(Comments.class);
    }

    public TreeNode getCommentsByPostId(int postId, int userId) {
        TreeNode root = new DefaultTreeNode("Comments", null);
        root.setExpanded(true);

        List<Object[]> topLevelComments = em.createNativeQuery("select comments.comment_id, comments.user_id, comments.post_id, bodytext, reply_to_comment_id,comment_date,  \n" +
"coalesce(upvotecount,0) as upvotes,coalesce(downvotecount,0) as downvotes ,  coalesce(upvotecount,0)  - coalesce(downvotecount,0) as netvotes, \n" +
"votes.value, alias, has_link  from newsgames.comments\n" +
"left join (select comment_id, count(value) as upvotecount from newsgames.votes where value = 'up' group by comment_id) as upvotes\n" +
"on comments.comment_id = upvotes.comment_id\n" +
"left join\n" +
"(select comment_id, count(value) as downvotecount from newsgames.votes where value = 'down' group by comment_id) as downvotes\n" +
"on comments.comment_id = downvotes.comment_id\n" +
"left join newsgames.aliases on comments.post_id = aliases.post_id and comments.user_id = aliases.user_id\n" +
"left join (select * from newsgames.votes where user_id = ?) as votes on comments.comment_id = votes.comment_id\n" +
"where comments.post_id = ? and reply_to_comment_id is null\n"
                + "order by netvotes desc, comment_date desc").setParameter(1, userId).setParameter(2, postId).getResultList();

        Object[] singleTopLevelComment;
        for (int i = 0; i < topLevelComments.size(); i++) {
            singleTopLevelComment = topLevelComments.get(i);
            CommentsTreeNodeInfo info = new CommentsTreeNodeInfo(
                    (Integer) singleTopLevelComment[0],
                    (Integer) singleTopLevelComment[1],
                    (Integer) singleTopLevelComment[2],
                    singleTopLevelComment[3],
                    singleTopLevelComment[4],
                    (Date) singleTopLevelComment[5],
                    (Long) singleTopLevelComment[6],
                    (Long) singleTopLevelComment[7],
                    (Long) singleTopLevelComment[8],
                    (String) singleTopLevelComment[9],
                    (String) singleTopLevelComment[10],
                   Double.valueOf((String)singleTopLevelComment[11]));
            TreeNode newNode = new DefaultTreeNode(info, root);
            newNode.setExpanded(true);
            if (info.getCommentId() != -1 || info.getCommentId() == null) {
                getChildrenOfComment(info.getPostId(), info.getCommentId(), userId, newNode);
            }
        }

        return root;
    }

    private void getChildrenOfComment(int postId, int commentId, int userId, TreeNode parent) {

        List<Object[]> topLevelComments = em.createNativeQuery("select comments.comment_id, comments.user_id, comments.post_id, bodytext, reply_to_comment_id,comment_date,  \n" +
"coalesce(upvotecount,0) as upvotes,coalesce(downvotecount,0) as downvotes ,  coalesce(upvotecount,0)  - coalesce(downvotecount,0) as netvotes, \n" +
" votes.value, alias, has_link  from newsgames.comments\n" +
"left join (select comment_id, count(value) as upvotecount from newsgames.votes where value = 'up' group by comment_id) as upvotes\n" +
"on comments.comment_id = upvotes.comment_id\n" +
"left join\n" +
"(select comment_id, count(value) as downvotecount from newsgames.votes where value = 'down' group by comment_id) as downvotes\n" +
"on comments.comment_id = downvotes.comment_id\n" +
"left join newsgames.aliases on comments.post_id = aliases.post_id and comments.user_id = aliases.user_id\n" +
"left join (select * from newsgames.votes where user_id = ?) as votes on comments.comment_id = votes.comment_id\n" +
"where comments.post_id = ? and reply_to_comment_id =?\n" +
"order by netvotes desc, comment_date desc").setParameter(1, userId).setParameter(2, postId).setParameter(3, commentId).getResultList();

        Object[] singleTopLevelComment;
        for (int i = 0; i < topLevelComments.size(); i++) {
            singleTopLevelComment = topLevelComments.get(i);
            CommentsTreeNodeInfo info = new CommentsTreeNodeInfo(
                    (Integer) singleTopLevelComment[0],
                    (Integer) singleTopLevelComment[1],
                    (Integer) singleTopLevelComment[2],
                    singleTopLevelComment[3],
                    singleTopLevelComment[4],
                    (Date) singleTopLevelComment[5],
                    (Long) singleTopLevelComment[6],
                    (Long) singleTopLevelComment[7],
                    (Long) singleTopLevelComment[8],
                    (String) singleTopLevelComment[9],
                    (String) singleTopLevelComment[10],
                     Double.valueOf((String)singleTopLevelComment[11]));
            TreeNode newNode = new DefaultTreeNode(info, parent);
            newNode.setExpanded(true);
            if (info.getCommentId() != -1 || info.getCommentId() == null) {
                getChildrenOfComment(info.getPostId(), info.getCommentId(), userId, newNode);
            }
        }

    }

    public void insertComment(String userID, String bodytext, String postID, String replyToCommentID) {
        String hasLink = "1";
        if(CommentsTreeNodeInfo.hasLink(bodytext)){
            hasLink = "2";
        }
        
        em.createNativeQuery("insert into newsgames.comments (user_id, bodytext, post_id, reply_to_comment_id, has_link) VALUES(?, ?, ?, ?, ?)").setParameter(1, userID).setParameter(2, bodytext).setParameter(3, postID).setParameter(4, replyToCommentID).setParameter(5, hasLink).executeUpdate();

    }
    
    public void deleteComment( String commentId){
        em.createNativeQuery("update newsgames.comments set bodytext='[This comment was deleted]' where comment_id=?").setParameter(1, commentId).executeUpdate();
        
    }
    
    public void editComment(String commentId, String comment){
        em.createNativeQuery("update newsgames.comments set bodytext=? where comment_id=?").setParameter(1, comment).setParameter(2, commentId).executeUpdate();
    }

}
