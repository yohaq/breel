/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.PollVotes;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class PollVotesFacade extends AbstractFacade<PollVotes> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PollVotesFacade() {
        super(PollVotes.class);
    }
    
     public void castVote(int userid, int postid, int seqno){
       
        
         em.createNativeQuery("INSERT INTO newsgames.poll_votes (user_id, post_id, seqno) VALUES (?, ?,?)" +
"  ON DUPLICATE KEY UPDATE seqno=?;").setParameter(1, userid).setParameter(2, postid).setParameter(3, seqno).setParameter(4, seqno).executeUpdate();
         
        
    }
     public List<PollVotes> getVoteForPostByUser(int userid, int postid){
       
        
        List<PollVotes> result = em.createNativeQuery("SELECT * FROM newsgames.poll_votes where user_id= ? and post_id = ?", PollVotes.class).setParameter(1, userid).setParameter(2, postid).getResultList();
        return result;
         
        
    }
     
     
    
}
