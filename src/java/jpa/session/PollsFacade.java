/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Polls;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class PollsFacade extends AbstractFacade<Polls> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PollsFacade() {
        super(Polls.class);
    }
    
    public List<Polls> getPollsById(int id){
        return em.createNamedQuery("Polls.findByPostId").setParameter("postId", id).getResultList();
    }
}
