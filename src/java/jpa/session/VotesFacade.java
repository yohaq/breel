/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Votes;

/**
 *
 * @author yousufhaque
 */
@Stateless
public class VotesFacade extends AbstractFacade<Votes> {
    @PersistenceContext(unitName = "BReelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VotesFacade() {
        super(Votes.class);
    }
    
    public void castVote(String userId, String commentId, String voteValue){
        em.createNativeQuery("insert into newsgames.votes (comment_id, user_id, value) VALUES(?, ?, ?)ON DUPLICATE KEY UPDATE value=?;").setParameter(1, commentId).setParameter(2, userId).setParameter(3, voteValue).setParameter(4, voteValue).executeUpdate();
    }
    
    public void deleteVote(String userId, String commentId ){
        em.createNativeQuery("delete from newsgames.votes where user_id=? and comment_id=?").setParameter(1, userId).setParameter(2, commentId).executeUpdate();
    }
}
