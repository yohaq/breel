/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "multipliers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Multipliers.findAll", query = "SELECT m FROM Multipliers m"),
    @NamedQuery(name = "Multipliers.findBySchemeName", query = "SELECT m FROM Multipliers m WHERE m.schemeName = :schemeName"),
    @NamedQuery(name = "Multipliers.findByDescription", query = "SELECT m FROM Multipliers m WHERE m.description = :description"),
    @NamedQuery(name = "Multipliers.findByVote", query = "SELECT m FROM Multipliers m WHERE m.vote = :vote"),
    @NamedQuery(name = "Multipliers.findByLink", query = "SELECT m FROM Multipliers m WHERE m.link = :link")})
public class Multipliers implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "scheme_name")
    private String schemeName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vote")
    private int vote;
    @Basic(optional = false)
    @NotNull
    @Column(name = "link")
    private int link;

    public Multipliers() {
    }

    public Multipliers(String schemeName) {
        this.schemeName = schemeName;
    }

    public Multipliers(String schemeName, String description, int vote, int link) {
        this.schemeName = schemeName;
        this.description = description;
        this.vote = vote;
        this.link = link;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public int getLink() {
        return link;
    }

    public void setLink(int link) {
        this.link = link;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (schemeName != null ? schemeName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Multipliers)) {
            return false;
        }
        Multipliers other = (Multipliers) object;
        if ((this.schemeName == null && other.schemeName != null) || (this.schemeName != null && !this.schemeName.equals(other.schemeName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Multipliers[ schemeName=" + schemeName + " ]";
    }
    
}
