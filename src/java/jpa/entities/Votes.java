/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "votes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Votes.findAll", query = "SELECT v FROM Votes v"),
    @NamedQuery(name = "Votes.findByCommentId", query = "SELECT v FROM Votes v WHERE v.votesPK.commentId = :commentId"),
    @NamedQuery(name = "Votes.findByUserId", query = "SELECT v FROM Votes v WHERE v.votesPK.userId = :userId"),
    @NamedQuery(name = "Votes.findByValue", query = "SELECT v FROM Votes v WHERE v.value = :value")})
public class Votes implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VotesPK votesPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "value")
    private String value;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;
    @JoinColumn(name = "comment_id", referencedColumnName = "comment_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Comments comments;

    public Votes() {
    }

    public Votes(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public Votes(VotesPK votesPK, String value) {
        this.votesPK = votesPK;
        this.value = value;
    }

    public Votes(int commentId, int userId) {
        this.votesPK = new VotesPK(commentId, userId);
    }

    public VotesPK getVotesPK() {
        return votesPK;
    }

    public void setVotesPK(VotesPK votesPK) {
        this.votesPK = votesPK;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (votesPK != null ? votesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Votes)) {
            return false;
        }
        Votes other = (Votes) object;
        if ((this.votesPK == null && other.votesPK != null) || (this.votesPK != null && !this.votesPK.equals(other.votesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Votes[ votesPK=" + votesPK + " ]";
    }
    
}
