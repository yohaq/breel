/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "poll_votes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PollVotes.findAll", query = "SELECT p FROM PollVotes p"),
    @NamedQuery(name = "PollVotes.findByUserId", query = "SELECT p FROM PollVotes p WHERE p.pollVotesPK.userId = :userId"),
    @NamedQuery(name = "PollVotes.findByPostId", query = "SELECT p FROM PollVotes p WHERE p.pollVotesPK.postId = :postId"),
    @NamedQuery(name = "PollVotes.findBySeqno", query = "SELECT p FROM PollVotes p WHERE p.seqno = :seqno")
   })
public class PollVotes implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PollVotesPK pollVotesPK;
    @Column(name = "seqno")
    private Integer seqno;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;
    @JoinColumn(name = "post_id", referencedColumnName = "post_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Posts posts;

    public PollVotes() {
    }

    public PollVotes(PollVotesPK pollVotesPK) {
        this.pollVotesPK = pollVotesPK;
    }

    public PollVotes(int userId, int postId) {
        this.pollVotesPK = new PollVotesPK(userId, postId);
    }

    public PollVotesPK getPollVotesPK() {
        return pollVotesPK;
    }

    public void setPollVotesPK(PollVotesPK pollVotesPK) {
        this.pollVotesPK = pollVotesPK;
    }

    public Integer getSeqno() {
        return seqno;
    }

    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pollVotesPK != null ? pollVotesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PollVotes)) {
            return false;
        }
        PollVotes other = (PollVotes) object;
        if ((this.pollVotesPK == null && other.pollVotesPK != null) || (this.pollVotesPK != null && !this.pollVotesPK.equals(other.pollVotesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.PollVotes[ pollVotesPK=" + pollVotesPK + " ]";
    }

}
