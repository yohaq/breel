/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author yousufhaque
 */
@Embeddable
public class PollsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "post_id")
    private int postId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seqno")
    private int seqno;

    public PollsPK() {
    }

    public PollsPK(int postId, int seqno) {
        this.postId = postId;
        this.seqno = seqno;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getSeqno() {
        return seqno;
    }

    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) postId;
        hash += (int) seqno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PollsPK)) {
            return false;
        }
        PollsPK other = (PollsPK) object;
        if (this.postId != other.postId) {
            return false;
        }
        if (this.seqno != other.seqno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.PollsPK[ postId=" + postId + ", seqno=" + seqno + " ]";
    }
    
}
