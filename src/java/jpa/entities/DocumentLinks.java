/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "document_links")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentLinks.findAll", query = "SELECT d FROM DocumentLinks d"),
    @NamedQuery(name = "DocumentLinks.findByCommentId", query = "SELECT d FROM DocumentLinks d WHERE d.commentId = :commentId")})
public class DocumentLinks implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "comment_id")
    private Integer commentId;
    @JoinColumn(name = "document_id", referencedColumnName = "document_id")
    @ManyToOne
    private Documents documentId;
    @JoinColumn(name = "comment_id", referencedColumnName = "comment_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Comments comments;

    public DocumentLinks() {
    }

    public DocumentLinks(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Documents getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Documents documentId) {
        this.documentId = documentId;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentId != null ? commentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentLinks)) {
            return false;
        }
        DocumentLinks other = (DocumentLinks) object;
        if ((this.commentId == null && other.commentId != null) || (this.commentId != null && !this.commentId.equals(other.commentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.DocumentLinks[ commentId=" + commentId + " ]";
    }
    
}
