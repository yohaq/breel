/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "aliases")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aliases.findAll", query = "SELECT a FROM Aliases a"),
    @NamedQuery(name = "Aliases.findByAlias", query = "SELECT a FROM Aliases a WHERE a.aliasesPK.alias = :alias"),
    @NamedQuery(name = "Aliases.findByUserId", query = "SELECT a FROM Aliases a WHERE a.aliasesPK.userId = :userId"),
    @NamedQuery(name = "Aliases.findByPostId", query = "SELECT a FROM Aliases a WHERE a.aliasesPK.postId = :postId")})
public class Aliases implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AliasesPK aliasesPK;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;
    @JoinColumn(name = "post_id", referencedColumnName = "post_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Posts posts;

    public Aliases() {
    }

    public Aliases(AliasesPK aliasesPK) {
        this.aliasesPK = aliasesPK;
    }

    public Aliases(String alias, int userId, int postId) {
        this.aliasesPK = new AliasesPK(alias, userId, postId);
    }

    public AliasesPK getAliasesPK() {
        return aliasesPK;
    }

    public void setAliasesPK(AliasesPK aliasesPK) {
        this.aliasesPK = aliasesPK;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aliasesPK != null ? aliasesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aliases)) {
            return false;
        }
        Aliases other = (Aliases) object;
        if ((this.aliasesPK == null && other.aliasesPK != null) || (this.aliasesPK != null && !this.aliasesPK.equals(other.aliasesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Aliases[ aliasesPK=" + aliasesPK + " ]";
    }
    
}
