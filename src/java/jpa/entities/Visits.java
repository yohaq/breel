/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "visits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Visits.findAll", query = "SELECT v FROM Visits v"),
    @NamedQuery(name = "Visits.findByUserId", query = "SELECT v FROM Visits v WHERE v.visitsPK.userId = :userId"),
    @NamedQuery(name = "Visits.findByDatetime", query = "SELECT v FROM Visits v WHERE v.visitsPK.datetime = :datetime")})
public class Visits implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VisitsPK visitsPK;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Users users;

    public Visits() {
    }

    public Visits(VisitsPK visitsPK) {
        this.visitsPK = visitsPK;
    }

    public Visits(int userId, Date datetime) {
        this.visitsPK = new VisitsPK(userId, datetime);
    }

    public VisitsPK getVisitsPK() {
        return visitsPK;
    }

    public void setVisitsPK(VisitsPK visitsPK) {
        this.visitsPK = visitsPK;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (visitsPK != null ? visitsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visits)) {
            return false;
        }
        Visits other = (Visits) object;
        if ((this.visitsPK == null && other.visitsPK != null) || (this.visitsPK != null && !this.visitsPK.equals(other.visitsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Visits[ visitsPK=" + visitsPK + " ]";
    }
    
}
