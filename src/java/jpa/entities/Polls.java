/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "polls")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Polls.findAll", query = "SELECT p FROM Polls p"),
    @NamedQuery(name = "Polls.findByPostId", query = "SELECT p FROM Polls p WHERE p.pollsPK.postId = :postId order by p.pollsPK.seqno asc"),
    @NamedQuery(name = "Polls.findBySeqno", query = "SELECT p FROM Polls p WHERE p.pollsPK.seqno = :seqno"),
    @NamedQuery(name = "Polls.findByTitle", query = "SELECT p FROM Polls p WHERE p.title = :title")})
public class Polls implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PollsPK pollsPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @JoinColumn(name = "post_id", referencedColumnName = "post_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Posts posts;

    public Polls() {
    }

    public Polls(PollsPK pollsPK) {
        this.pollsPK = pollsPK;
    }

    public Polls(PollsPK pollsPK, String title) {
        this.pollsPK = pollsPK;
        this.title = title;
    }

    public Polls(int postId, int seqno) {
        this.pollsPK = new PollsPK(postId, seqno);
    }

    public PollsPK getPollsPK() {
        return pollsPK;
    }

    public void setPollsPK(PollsPK pollsPK) {
        this.pollsPK = pollsPK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pollsPK != null ? pollsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Polls)) {
            return false;
        }
        Polls other = (Polls) object;
        if ((this.pollsPK == null && other.pollsPK != null) || (this.pollsPK != null && !this.pollsPK.equals(other.pollsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Polls[ pollsPK=" + pollsPK + " ]";
    }
    
    
    public int getSeqno(){
        return pollsPK.getSeqno();
    }
}
