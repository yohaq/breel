/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Calendar;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.validator.UrlValidator;

/**
 *
 * @author yousufhaque
 */
@Entity
@Table(name = "posts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Posts.findAll", query = "SELECT p FROM Posts p"),
    @NamedQuery(name = "Posts.findAllOrderedByDate", query = "SELECT p FROM Posts p where p.releaseDate <= CURRENT_DATE order by p.releaseDate desc"),
    @NamedQuery(name = "Posts.findByPostId", query = "SELECT p FROM Posts p WHERE p.postId = :postId"),
    @NamedQuery(name = "Posts.findByTitle", query = "SELECT p FROM Posts p WHERE p.title = :title"),
    @NamedQuery(name = "Posts.findByBody", query = "SELECT p FROM Posts p WHERE p.body = :body"),
    @NamedQuery(name = "Posts.findByLink", query = "SELECT p FROM Posts p WHERE p.link = :link"),
    @NamedQuery(name = "Posts.findByReleaseDate", query = "SELECT p FROM Posts p WHERE p.releaseDate = :releaseDate")})
public class Posts implements Serializable {

    @Size(max = 1000)
    @Column(name = "poll_question")
    private String pollQuestion;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "post_id")
    private Integer postId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3000)
    @Column(name = "body")
    private String body;
    @Size(max = 1000)
    @Column(name = "link")
    private String link;
    @Basic(optional = false)
    @NotNull
    @Column(name = "release_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "posts")
    private Collection<Polls> pollsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "posts")
    private Collection<Aliases> aliasesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "postId")
    private Collection<Comments> commentsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "posts")
    private Collection<PollVotes> pollVotesCollection;

    public Posts() {
    }

    public Posts(Integer postId) {
        this.postId = postId;
    }

    public Posts(Integer postId, String title, String body, Date releaseDate) {
        this.postId = postId;
        this.title = title;
        this.body = body;
        this.releaseDate = releaseDate;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public String getLinkBody() {
        String[] parts = body.split("\\s");

        String tempBodyText = body;
        UrlValidator validator = new UrlValidator();

        String linkifiedBodyText;
        for (String item : parts) {
            if (item.contains("http://") || item.contains("https://")) {
                if (validator.isValid(item)) {
                    tempBodyText = tempBodyText.replace(item, "<a href=\"" + item + "\">" + item + "</a> ");

                }
            } else if (validator.isValid("http://" + item)) {
                tempBodyText = tempBodyText.replace(item, "<a href=\"http://" + item + "\">" + item + "</a> ");

            }

        }
        linkifiedBodyText = tempBodyText;

        return linkifiedBodyText;
    }

    public String getBodyWithoutLink() {
        String finalText = body;
        while (finalText.contains("<a")) {
            String firstPiece = body.substring(0, body.indexOf("<a"));
            String linkText = body.substring(body.indexOf(">") + 1, body.indexOf("</a>"));
            String lastPiece = body.substring(body.indexOf(">", body.indexOf("</a>")) + 1);
            finalText = firstPiece + linkText + lastPiece;
        }
        return finalText;
    }

    public String getBodyWithLink() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @XmlTransient
    public Collection<Polls> getPollsCollection() {
        return pollsCollection;
    }

    public void setPollsCollection(Collection<Polls> pollsCollection) {
        this.pollsCollection = pollsCollection;
    }

    @XmlTransient
    public Collection<Aliases> getAliasesCollection() {
        return aliasesCollection;
    }

    public void setAliasesCollection(Collection<Aliases> aliasesCollection) {
        this.aliasesCollection = aliasesCollection;
    }

    @XmlTransient
    public Collection<Comments> getCommentsCollection() {
        return commentsCollection;
    }

    public void setCommentsCollection(Collection<Comments> commentsCollection) {
        this.commentsCollection = commentsCollection;
    }

    @XmlTransient
    public Collection<PollVotes> getPollVotesCollection() {
        return pollVotesCollection;
    }

    public void setPollVotesCollection(Collection<PollVotes> pollVotesCollection) {
        this.pollVotesCollection = pollVotesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (postId != null ? postId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Posts)) {
            return false;
        }
        Posts other = (Posts) object;
        if ((this.postId == null && other.postId != null) || (this.postId != null && !this.postId.equals(other.postId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Posts[ postId=" + postId + " ]";
    }

    public String getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public boolean isOlderThanAWeek() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(releaseDate);
        int numDaysValid = 7;
        if (postId == 1) {
            numDaysValid = 14;
        }

        if (postId == 2) {
            numDaysValid = 4;
        }

        if (postId == 3) {
            numDaysValid = 6;
        }

        if (postId == 4) {
            numDaysValid = 10;
        }
        if (postId == 5) {
            numDaysValid = 6;
        }
        if (postId == 6) {
            return false;
        }
        cal.add(Calendar.DATE, numDaysValid);
        cal.set(HOUR, 0);
        cal.set(MINUTE, 0);
        cal.set(SECOND, 0);
        cal.set(MILLISECOND, 0);

        Date OneWeekAfterReleaseDate = cal.getTime();

        return (new Date().after(OneWeekAfterReleaseDate));
    }
    /**
     * int numDaysValid = 7;
        if (postId == 1) {
            numDaysValid = 11;
        }

        if (postId == 2) {
            numDaysValid = 5;
        }

        if (postId == 3) {
            numDaysValid = 4;
        }

        if (postId == 4) {
            numDaysValid = 3;
        }
        if (postId == 5) {
            numDaysValid = 7;
        }
        if (postId == 6) {
            return true;
        }
     */

}
