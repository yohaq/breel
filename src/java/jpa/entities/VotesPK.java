/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author yousufhaque
 */
@Embeddable
public class VotesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "comment_id")
    private int commentId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private int userId;

    public VotesPK() {
    }

    public VotesPK(int commentId, int userId) {
        this.commentId = commentId;
        this.userId = userId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) commentId;
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VotesPK)) {
            return false;
        }
        VotesPK other = (VotesPK) object;
        if (this.commentId != other.commentId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.VotesPK[ commentId=" + commentId + ", userId=" + userId + " ]";
    }
    
}
