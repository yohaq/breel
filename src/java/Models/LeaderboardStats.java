/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author yousufhaque
 */
public class LeaderboardStats {
    
    private Integer userId;
    private String email;
    private Long numComments;
    private Double totalVotes;
    
    public LeaderboardStats(Integer userId, String email, Long numComments, Double totalVotes){
        this.userId = userId;
        this.numComments = numComments;
        this.email = email;
        this.totalVotes = totalVotes;
        
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getNumComments() {
        return numComments;
    }

    public void setNumComments(Long numComments) {
        this.numComments = numComments;
    }

    public Double getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Double totalVotes) {
        this.totalVotes = totalVotes;
    }

 

  
    
    
    
}
