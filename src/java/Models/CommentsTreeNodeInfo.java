/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import org.apache.commons.validator.UrlValidator;

/**
 *
 * @author yousufhaque
 */
public class CommentsTreeNodeInfo {

    private Integer commentId;
    private Integer userId;
    private Integer postId;
    private String bodytext = null;
    private Integer replyToCommentId;
    private Long upvotes;
    private Long downvotes;
    private Long netvotes;
    private Date commentCreationDate;
    private String voteValue;
    private String userName;
    private Boolean hasLink;
    private String linkifiedBodyText;
    private Double multiplier;

    public CommentsTreeNodeInfo(
            Integer commentId,
            Integer userId,
            Integer postId,
            Object bodytext,
            Object replyToCommentId,
            Date date,
            Long upvotes,
            Long downvotes,
            Long netvotes,
            String voteValue,
            String userName,
            Double hasLink) {
        this.replyToCommentId = new Integer(-1);
        this.commentId = commentId;
        this.userId = userId;
        this.postId = postId;
        if (bodytext != null) {
            this.bodytext = (String) bodytext;
        }
        if (replyToCommentId != null) {
            this.replyToCommentId = (Integer) replyToCommentId;
        }
        this.upvotes = upvotes;
        this.downvotes = downvotes;
        this.netvotes = netvotes;
        this.commentCreationDate = date;
        this.voteValue = voteValue;
        this.userName = userName;
        this.multiplier = hasLink;
        convertToLinksIfAnyInText();
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getBodytext() {
        return bodytext;
    }

    public String getLinkifiedBodyText() {

        return linkifiedBodyText;
    }

    private String convertToLinksIfAnyInText() {
        String[] parts = bodytext.split("\\s");

        String tempBodyText = bodytext;
        UrlValidator validator = new UrlValidator();

        hasLink = false;
        for (String item : parts) {
            if (item.contains("http://") || item.contains("https://")) {
                if (validator.isValid(item)) {
                    tempBodyText = tempBodyText.replace(item, "<a href=\"" + item + "\">" + item + "</a> ");
                    hasLink = true;
                }
            } else if (validator.isValid("http://" + item)) {
                tempBodyText = tempBodyText.replace(item, "<a href=\"http://" + item + "\">" + item + "</a> ");
                hasLink = true;
            }

        }
        linkifiedBodyText = tempBodyText;

        return linkifiedBodyText;
    }

    public static boolean hasLink(String comment) {
        String[] parts = comment.split("\\s");

        UrlValidator validator = new UrlValidator();

        for (String item : parts) {

            if (item.contains("http://") || item.contains("https://")) {
                if (validator.isValid(item)) {

                    return true;
                }
            } else if (validator.isValid("http://" + item)) {

                return true;
            }
        }
        return false;
    }

    public void setBodytext(String bodytext) {
        this.bodytext = bodytext;
    }

    public Integer getReplyToCommentId() {
        return replyToCommentId;
    }

    public void setReplyToCommentId(Integer replyToCommentId) {
        this.replyToCommentId = replyToCommentId;
    }

    public Long getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(Long upvotes) {
        this.upvotes = upvotes;
    }

    public Long getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(Long downvotes) {
        this.downvotes = downvotes;
    }

    public Long getNetvotes() {
        return netvotes;
    }

    public String getDisplayNetvotes() {
        if (Math.abs(netvotes) > 1 || netvotes == 0) {
            return netvotes + " points";
        }
        return netvotes + " point";
    }

    public void setNetvotes(Long netvotes) {
        this.netvotes = netvotes;
    }

    public Date getCommentCreationDate() {
        return commentCreationDate;
    }

    public void setCommentCreationDate(Date commentCreationDate) {
        this.commentCreationDate = commentCreationDate;
    }

    public String getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(String voteValue) {
        this.voteValue = voteValue;
    }

    public String getUserName() {
        return userName;
    }

    public String getDisplayUserName(int userId) {
        if (userId == this.userId) {
            return "me";
        } else {
            return userName;
        }
    }

    public String getUsernameClass(int userId) {
        if (userId == this.userId) {
            return "me";
        } else {
            return "other";
        }
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String isUpvoted() {
        if (voteValue == null) {
            return "";
        }
        if (voteValue.equals("up")) {
            return "checked";
        }
        return "";
    }

    public String isDownvoted() {
        if (voteValue == null) {
            return "";
        }
        if (voteValue.equals("down")) {
            return "checked";
        }
        return "";
    }

    public Integer getNetVotes() {
        Double tempMultiplier = 1.0;
        if (hasLink) {
            tempMultiplier = multiplier;
        }
        return Double.valueOf(Math.ceil(netvotes * tempMultiplier)).intValue();
    }
    
    public boolean isOwnerOfComment(int userId){
        return userId==this.userId && isNotDeleted();
    }
    
    public boolean isNotDeleted(){
        return !bodytext.equals("[This comment was deleted]");
    }
}
